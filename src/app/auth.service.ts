import { Injectable, EventEmitter } from '@angular/core';


@Injectable()
export class AuthService {

  constructor() { 
    this.setAuthentification("admin","admin");
    this.setIsAuthentification(true);
  }
  private auth : string;
  private isAuth: boolean;
  public username: string;
  change: EventEmitter<boolean> = new EventEmitter();

  public getAuthentification(): string {
    return this.auth;
  }

  public setAuthentification(username: string, password: string) {
    this.username = username;
    this.auth = btoa(username+":"+password);
    this.isAuth = true;
  }

  public isAuthenticated(): boolean {
    return this.isAuth;
  }

  public setIsAuthentification(ok : boolean)
  {
    this.isAuth = ok;
    this.change.emit(ok);
  }

  public cleanAuth()
  {
    this.setIsAuthentification(false);
  }



}
