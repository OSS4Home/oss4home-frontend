import { Component } from '@angular/core';
import { ControllerService } from './api/index';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(controllerService: ControllerService)
  {
    //controllerService.configuration.withCredentials = true;
    //controllerService.configuration.username = "admin";
    //controllerService.configuration.password = "admin";
  }
}
