import { Component } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent {
  public login: boolean;
  public username: string;
  isCollapsed = true;

  constructor(protected authService: AuthService, protected router: Router) {
    this.login = authService.isAuthenticated();
    authService.change.subscribe(isAuth => this.onChange(isAuth));
  }

  toggleMenu() {
    this.isCollapsed = !this.isCollapsed;
  }
  onChange(login: boolean) {
    this.login = login;
    this.username = this.authService.username;
  }
  onLogout(){
    this.authService.cleanAuth();
    this.router.navigate(['/login']);
  }

}
