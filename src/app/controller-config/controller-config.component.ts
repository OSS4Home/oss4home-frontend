import { Component, OnInit } from '@angular/core';
import { ControllerService, Controllers, Controller } from '../api/index';

@Component({
  selector: 'app-controller-config',
  templateUrl: './controller-config.component.html',
  styleUrls: ['./controller-config.component.css']
})
export class ControllerConfigComponent implements OnInit {

  public controllers : Controllers;
  public controllerService : ControllerService;
  public errorMessage: Error;

  constructor(controllerService : ControllerService) { 
    this.controllers = new Array<Controller>(0);
    this.controllerService = controllerService;
    this.loadController();
  }

  ngOnInit() {
  }


  onDelete(id : number)
  {
    this.controllerService.deleteController(String(id)).subscribe(
      response => this.loadController(), 
      error => this.errorMessage = <any>error
);  

  }

  loadController()
  {
    this.controllerService.getControllers().subscribe((controllers : Controllers)=>{
      this.controllers = controllers;
    })
  }

}
