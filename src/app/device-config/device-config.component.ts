import { Component, OnInit } from '@angular/core';
import { Devices, DevicesService, Device } from '../api/index';

@Component({
  selector: 'app-device-config',
  templateUrl: './device-config.component.html',
  styleUrls: ['./device-config.component.css']
})
export class DeviceConfigComponent implements OnInit {

  public devices : Devices;
  public errorMessage: Error;

  constructor(public deviceService : DevicesService) { 
    this.devices = new Array<Device>(0);
    this.loadDevices();
  }


  ngOnInit() {
  }


  loadDevices()
  {
    this.deviceService.getDevices().subscribe((devices : Devices)=>{
      this.devices = devices;
    })
  }

}
