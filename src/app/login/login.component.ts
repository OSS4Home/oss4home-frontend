import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public username: string;
  public password: string;
  public save: boolean;
  public error: string;

  constructor(protected httpClient: HttpClient, protected authService: AuthService, protected router : Router) {
    this.error = null;

   }

  ngOnInit() {
  }

  onSubmit() {

    let basePath = 'http://localhost:8080/rest/ccapi/v2';
    let headers = new HttpHeaders();
    this.authService.setAuthentification(this.username,this.password);


    headers = headers.set("Accept", 'application/json');
    //headers = headers.set("Authorization", "Basic " + this.authService.getAuthentification())
    
    

    this.httpClient.get<any>(`${basePath}/controllers`,
      {
        headers: headers
        //withCredentials: true,
      }
    ).subscribe(
      data => this.login(),
      error => this.onError()
      );

  }

  login() {
    this.error = null;
    this.authService.setIsAuthentification(true);
    this.router.navigate(['/']);
  }

  onError(){
    this.error = "Bitte geben Sie gültige Anmeldeinformationen an."
    this.authService.cleanAuth();
    
  }



}
