import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NavigationComponent } from './navigation/navigation.component';
import { ControllerConfigComponent } from './controller-config/controller-config.component';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { ApiModule } from './api/api.module';
import { ControllerAddComponent } from './controller-add/controller-add.component';
import { FormsModule } from '@angular/forms';
import { LoginComponent } from './login/login.component';
import { AuthGuardService } from './auth-guard.service';
import { AuthService } from './auth.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './auth-interceptor.service';
import { SettingsComponent } from './settings/settings.component';
import { DeviceConfigComponent } from './device-config/device-config.component';


const appRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    data: { title: 'login' }
  },
  {
    path: 'controller-add',
    component: ControllerAddComponent,
    data: { title: 'Heroes List' },
    canActivate: [AuthGuardService] 
  },
  {
    path: 'controller-config',
    component: ControllerConfigComponent,
    data: { title: 'Heroes List' },
    canActivate: [AuthGuardService] 
  },
  {
    path: 'settings',
    component: SettingsComponent,
    data: { title: 'Settings' },
    canActivate: [AuthGuardService] 
  },
  {
    path: 'device-config',
    component: DeviceConfigComponent,
    data: { title: 'Device Config' },
    canActivate: [AuthGuardService] 
  },
  { path: '',
    redirectTo: '/settings',
    pathMatch: 'full'
  },
  { path: 'controller-change/:id', component: ControllerAddComponent,canActivate: [AuthGuardService]  }
  //{ path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    ControllerConfigComponent,
    ControllerAddComponent,
    LoginComponent,
    SettingsComponent,
    DeviceConfigComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    NgbModule,
    BrowserModule,
    ApiModule,
    FormsModule
  ],
  providers: [AuthInterceptor,{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },AuthGuardService,AuthService],
  bootstrap: [AppComponent],
})
export class AppModule { }
