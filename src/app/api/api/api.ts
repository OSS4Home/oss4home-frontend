export * from './controller.service';
import { ControllerService } from './controller.service';
export * from './devices.service';
import { DevicesService } from './devices.service';
export * from './groups.service';
import { GroupsService } from './groups.service';
export const APIS = [ControllerService, DevicesService, GroupsService];
