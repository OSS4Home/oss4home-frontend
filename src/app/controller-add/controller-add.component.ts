import { Component, OnInit } from '@angular/core';
import { ControllerService, UpdateController, Error, Controller } from '../api/index';
import { HttpResponse } from '@angular/common/http/src/response';
import { HttpResponseBase } from '@angular/common/http';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-controller-add',
  templateUrl: './controller-add.component.html',
  styleUrls: ['./controller-add.component.css']
})
export class ControllerAddComponent implements OnInit {

  public controller: UpdateController = {
    connectionString: "",
    name: "",
    active: true
  }
  public controllerService: ControllerService;
  public router: Router;
  public errorMessage: Error;
  id: string;
  button :string;
  private sub: any;

  constructor(controllerService: ControllerService, router: Router, private route: ActivatedRoute) {
    this.controllerService = controllerService;
    this.router = router;
  }

  onSubmit() {
    if (this.id === null) {
      this.controllerService.addController(this.controller).subscribe(
        response => this.onStatus(<any>response),
        error => this.errorMessage = <any>error
      );
    }
    else{
      this.controllerService.changeController(this.id,this.controller).subscribe(
                response => this.onStatus(<any>response), 
                error => this.errorMessage = <any>error
        );
      }
  }

  onLoadChange() {
    this.controllerService.getController(this.id).subscribe((controller: Controller) => {
      this.controller.active = controller.active;
      this.controller.connectionString = controller.connectionString;
      this.controller.name = controller.name;
    });
  }

  onStatus(respone: HttpResponse<any>) {
    this.router.navigate(['/controller-config']);
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      if (params['id'] == null) {
        this.id = null;
        this.button = "Add";
      }
      else {
        this.id = params['id']; // (+) converts string 'id' to a number
        this.button = "Change";
        this.onLoadChange();
      }
      // In a real app: dispatch action to load the details here.
    });
  }

}
